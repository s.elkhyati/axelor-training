package com.axelor.apps.event.db.repo;

import com.axelor.apps.event.db.Event;
import com.axelor.db.JpaRepository;
import com.axelor.db.Query;

import com.axelor.apps.event.db.repo.EventRepository;
import java.time.Duration;

public class EventRepository extends JpaRepository<Event> {

	public EventRepository() {
		super(Event.class);
	}

	public Event findByCode(String code) {
		return Query.of(Event.class)
				.filter("self.code = :code")
				.bind("code", code)
				.fetchOne();
	}

	public final static Integer NOT_STARTED_YET_EVENT = 0;
	public final static Integer IN_PROGRESS_EVENT = 1;
	public final static Integer FINISHED_EVENT = 2;
	public final static Integer CANCELED_EVENT = 3;
}

