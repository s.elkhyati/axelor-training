package com.axelor.apps.event.db;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.axelor.auth.db.AuditableModel;
import com.axelor.auth.db.User;
import com.axelor.db.annotations.NameColumn;
import com.axelor.db.annotations.Sequence;
import com.axelor.db.annotations.VirtualColumn;
import com.axelor.db.annotations.Widget;
import com.google.common.base.MoreObjects;

import com.axelor.apps.event.db.repo.EventRepository;
import java.time.Duration;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "EVENT_EVENT", indexes = { @Index(columnList = "code"), @Index(columnList = "user_entity") })
public class Event extends AuditableModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVENT_EVENT_SEQ")
	@SequenceGenerator(name = "EVENT_EVENT_SEQ", sequenceName = "EVENT_EVENT_SEQ", allocationSize = 1)
	private Long id;

	@Widget(readonly = true)
	@NameColumn
	@Sequence("event.seq")
	private String code;

	@Widget(title = "User")
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private User userEntity;

	@Widget(title = "State", readonly = true, selection = "event.event.state.select")
	private Integer state = EventRepository.NOT_STARTED_YET_EVENT;

	@NotNull
	private String subject;

	@Widget(title = "Duration", readonly = true)
	@VirtualColumn
	@Access(AccessType.PROPERTY)
	private LocalTime duration;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Type(type = "text")
	@NotNull
	private String content;

	@Widget(title = "Related to", selection = "event.event.related.to.ref.select")
	private String relatedToReference;

	@Widget(title = "Event start date")
	@NotNull
	private LocalDate eventDate;

	@Widget(title = "Event limit date")
	@NotNull
	private LocalDate eventLimitDate;

	@Widget(title = "Event start")
	@NotNull
	private LocalTime eventStartTime;

	@Widget(title = "Event end time")
	@NotNull
	private LocalTime eventEndTime;

	@Widget(title = "Attributes")
	@Basic(fetch = FetchType.LAZY)
	@Type(type = "json")
	private String attrs;

	public Event() {
	}

	public Event(String code) {
		this.code = code;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public User getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(User userEntity) {
		this.userEntity = userEntity;
	}

	public Integer getState() {
		return state == null ? 0 : state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public LocalTime getDuration() {
		try {
			duration = computeDuration();
		} catch (NullPointerException e) {
			Logger logger = LoggerFactory.getLogger(getClass());
			logger.error("NPE in function field: getDuration()", e);
		}
		return duration;
	}

	protected LocalTime computeDuration() {
		if (eventStartTime != null && eventEndTime != null){
		Duration duration = Duration.between(eventStartTime, eventEndTime);
		LocalTime time = LocalTime.of(0,0);
		return time.plus(duration);
		}
		return null;
	}

	public void setDuration(LocalTime duration) {
		this.duration = duration;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRelatedToReference() {
		return relatedToReference;
	}

	public void setRelatedToReference(String relatedToReference) {
		this.relatedToReference = relatedToReference;
	}

	public LocalDate getEventDate() {
		return eventDate;
	}

	public void setEventDate(LocalDate eventDate) {
		this.eventDate = eventDate;
	}

	public LocalDate getEventLimitDate() {
		return eventLimitDate;
	}

	public void setEventLimitDate(LocalDate eventLimitDate) {
		this.eventLimitDate = eventLimitDate;
	}

	public LocalTime getEventStartTime() {
		return eventStartTime;
	}

	public void setEventStartTime(LocalTime eventStartTime) {
		this.eventStartTime = eventStartTime;
	}

	public LocalTime getEventEndTime() {
		return eventEndTime;
	}

	public void setEventEndTime(LocalTime eventEndTime) {
		this.eventEndTime = eventEndTime;
	}

	public String getAttrs() {
		return attrs;
	}

	public void setAttrs(String attrs) {
		this.attrs = attrs;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (this == obj) return true;
		if (!(obj instanceof Event)) return false;

		final Event other = (Event) obj;
		if (this.getId() != null || other.getId() != null) {
			return Objects.equals(this.getId(), other.getId());
		}

		return false;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", getId())
			.add("code", getCode())
			.add("state", getState())
			.add("subject", getSubject())
			.add("relatedToReference", getRelatedToReference())
			.add("eventDate", getEventDate())
			.add("eventLimitDate", getEventLimitDate())
			.add("eventStartTime", getEventStartTime())
			.add("eventEndTime", getEventEndTime())
			.omitNullValues()
			.toString();
	}
}
