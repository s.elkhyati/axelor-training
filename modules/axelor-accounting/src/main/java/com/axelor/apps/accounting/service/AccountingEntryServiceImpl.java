package com.axelor.apps.accounting.service;

import com.axelor.apps.accounting.db.Account;
import com.axelor.apps.accounting.db.AccountingEntry;
import com.axelor.apps.accounting.db.EntryLine;
import com.axelor.apps.accounting.db.repo.AccountRepository;
import com.axelor.apps.accounting.db.repo.AccountingEntryRepository;
import com.axelor.apps.invoice.db.Invoice;
import com.axelor.apps.invoice.db.InvoiceLine;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;

public class AccountingEntryServiceImpl implements AccountingEntryService {

  @Inject protected AccountingEntryRepository accountingEntryRepository;
  @Inject protected AccountRepository accountRepository;
  @Inject protected AccountingService accountingService;

  @Override
  @Transactional
  public AccountingEntry generateAccountingEntry(Invoice invoice) {
    AccountingEntry accountingEntry = new AccountingEntry();
    accountingEntry.setOperationDate(LocalDate.now());
    Account account;
    BigDecimal clientDebit = BigDecimal.ZERO;
    BigDecimal vatCredit = BigDecimal.ZERO;
    for (InvoiceLine invoiceLine : invoice.getInvoiceLines()) {
      EntryLine entryLine = new EntryLine();
      entryLine.setAccountingEntry(accountingEntry);
      account = invoiceLine.getProduct().getCategory().getAccount();
      BigDecimal credit = invoiceLine.getSubtotalWithoutTax();
      BigDecimal vat =
          invoiceLine.getSubtotalTaxIncluded().subtract(invoiceLine.getSubtotalWithoutTax());
      entryLine.setAccount(account);
      entryLine.setCredit(credit);
      accountingEntry.addEntryLine(entryLine);
      //
      accountingService.setAccountSold(account, BigDecimal.ZERO, credit);
      //
      clientDebit = clientDebit.add(credit).add(vat);
      vatCredit = vatCredit.add(vat);
    }
    createVatEntryLine(accountingEntry, vatCredit);
    createClientEntryLine(accountingEntry, clientDebit);
    //
    accountingEntry.setTotalDebit(clientDebit);
    accountingEntry.setTotalCredit(clientDebit);
    return accountingEntryRepository.save(accountingEntry);
  }

  protected void createClientEntryLine(AccountingEntry accountingEntry, BigDecimal clientDebit) {
    EntryLine clientEntry = new EntryLine();
    clientEntry.setAccountingEntry(accountingEntry);
    Account clientAccount = accountRepository.findByCode(AccountRepository.CLIENT_ACCOUNT);
    clientEntry.setAccount(clientAccount);
    clientEntry.setDebit(clientDebit);
    accountingService.setAccountSold(clientAccount, clientDebit, BigDecimal.ZERO);
    accountingEntry.addEntryLine(clientEntry);
  }

  protected void createVatEntryLine(AccountingEntry accountingEntry, BigDecimal vatCredit) {
    EntryLine vatEntry = new EntryLine();
    vatEntry.setAccountingEntry(accountingEntry);
    Account vatAccount = accountRepository.findByCode(AccountRepository.VAT_ACCOUNT);
    vatEntry.setAccount(vatAccount);
    vatEntry.setCredit(vatCredit);
    accountingService.setAccountSold(vatAccount, BigDecimal.ZERO, vatCredit);
    accountingEntry.addEntryLine(vatEntry);
  }
}
