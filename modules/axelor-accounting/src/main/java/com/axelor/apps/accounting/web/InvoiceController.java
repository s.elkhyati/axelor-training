package com.axelor.apps.accounting.web;

import com.axelor.apps.accounting.service.AccountingEntryService;
import com.axelor.apps.invoice.db.Invoice;
import com.axelor.apps.invoice.db.InvoiceLine;
import com.axelor.apps.invoice.db.repo.InvoiceRepository;
import com.axelor.rpc.ActionRequest;
import com.axelor.rpc.ActionResponse;
import com.google.inject.Inject;
import java.util.List;

public class InvoiceController {

  @Inject protected AccountingEntryService accountingEntryService;

  public void generateAccountingEntries(ActionRequest request, ActionResponse response) {
    Invoice invoice = request.getContext().asType(Invoice.class);
    if (invoice != null) {
      if (invoice.getState() == InvoiceRepository.DRAFT_INVOICE) {
        response.setError("Please, validate invoice before generate accounting Entries");
        return;
      }
      List<InvoiceLine> invoiceLineList = invoice.getInvoiceLines();
      if (invoiceLineList == null) {
        response.setError("This invoice doesn't have invoice lines");
        return;
      }

      if (accountingEntryService.generateAccountingEntry(invoice) == null) {
        response.setError("This invoice doesn't have invoice lines");
        return;
      }
      response.setValue("state", InvoiceRepository.VENTILATED_INVOICE);
      response.setNotify("Accounting Entry has been created successfully");
    }
  }
}
