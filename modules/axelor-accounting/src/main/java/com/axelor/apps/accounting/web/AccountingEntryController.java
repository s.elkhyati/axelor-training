package com.axelor.apps.accounting.web;

import com.axelor.apps.accounting.db.AccountingEntry;
import com.axelor.apps.accounting.execption.BothCreditAndDebitAreZeroException;
import com.axelor.apps.accounting.service.AccountingService;
import com.axelor.apps.invoice.web.InvoiceLineController;
import com.axelor.rpc.ActionRequest;
import com.axelor.rpc.ActionResponse;
import com.google.inject.Inject;

public class AccountingEntryController extends InvoiceLineController {

  @Inject protected AccountingService accountingService;

  public void saveEntry(ActionRequest request, ActionResponse response) {
    AccountingEntry accountingEntry = request.getContext().asType(AccountingEntry.class);
    if (accountingEntry != null && accountingEntry.getEntryLines() != null) {
      if (!accountingEntry.getTotalCredit().equals(accountingEntry.getTotalDebit())) {
        response.setError("Total credit must equals total debit");
        return;
      }
      try {
        accountingService.updateAccountsSold(accountingEntry);
      } catch (BothCreditAndDebitAreZeroException e) {
        response.setError("In an entry line  debit or credit must be zero");
      }
    }
  }
}
