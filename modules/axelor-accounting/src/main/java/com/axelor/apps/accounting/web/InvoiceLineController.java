package com.axelor.apps.accounting.web;

import com.axelor.apps.invoice.db.InvoiceLine;
import com.axelor.apps.sale.db.Product;
import com.axelor.apps.sale.db.ProductCategory;
import com.axelor.rpc.ActionRequest;
import com.axelor.rpc.ActionResponse;


public class InvoiceLineController {


  public void setAccount(ActionRequest request, ActionResponse response) {
    InvoiceLine invoiceLine = request.getContext().asType(InvoiceLine.class);
    if (invoiceLine != null) {
      Product product = invoiceLine.getProduct();
      if (product == null) return;
      ProductCategory productCategory = product.getCategory();
      if (productCategory == null) return;
      response.setNotify(productCategory.getAccount().getName());
      response.setValue("account", productCategory.getAccount());
    }
  }
}