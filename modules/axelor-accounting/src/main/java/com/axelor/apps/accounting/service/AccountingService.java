package com.axelor.apps.accounting.service;

import com.axelor.apps.accounting.db.Account;
import com.axelor.apps.accounting.db.AccountingEntry;
import com.axelor.apps.accounting.execption.BothCreditAndDebitAreZeroException;
import java.math.BigDecimal;

public interface AccountingService {
  void updateAccountsSold(AccountingEntry accountingEntry)
      throws BothCreditAndDebitAreZeroException;

  Account setAccountSold(Account account, BigDecimal debit, BigDecimal credit);
}
