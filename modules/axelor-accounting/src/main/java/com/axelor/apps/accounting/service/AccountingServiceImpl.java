package com.axelor.apps.accounting.service;

import com.axelor.apps.accounting.db.Account;
import com.axelor.apps.accounting.db.AccountingEntry;
import com.axelor.apps.accounting.db.EntryLine;
import com.axelor.apps.accounting.db.repo.AccountRepository;
import com.axelor.apps.accounting.execption.BothCreditAndDebitAreZeroException;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import java.math.BigDecimal;
import java.util.List;

public class AccountingServiceImpl implements AccountingService {

  @Inject protected AccountRepository accountRepository;
  @Inject protected AccountingEntryService accountingEntryService;

  @Override
  @Transactional(rollbackOn = BothCreditAndDebitAreZeroException.class)
  public void updateAccountsSold(AccountingEntry accountingEntry)
      throws BothCreditAndDebitAreZeroException {
    List<EntryLine> entryLines = accountingEntry.getEntryLines();
    for (EntryLine entryLine : entryLines) {
      BigDecimal debit = entryLine.getDebit();
      BigDecimal credit = entryLine.getCredit();
      if (debit.compareTo(BigDecimal.ZERO) != 0 && credit.compareTo(BigDecimal.ZERO) != 0) {
        throw new BothCreditAndDebitAreZeroException();
      }
      Account account = entryLine.getAccount();
      setAccountSold(account, debit, credit);
      accountRepository.save(account);
    }
  }

  public Account setAccountSold(Account account, BigDecimal debit, BigDecimal credit) {
    BigDecimal initialSold = account.getSold();
    if (account.getSoldType() == AccountRepository.CREDIT_BALANCE) {
      credit = credit.add(initialSold);
    } else if (account.getSoldType() == AccountRepository.DEBIT_BALANCE) {
      debit = debit.add(initialSold);
    }
    BigDecimal sold = debit.subtract(credit);
    account.setSold(sold.abs());
    if (sold.compareTo(BigDecimal.ZERO) == 1) {
      account.setSoldType(AccountRepository.DEBIT_BALANCE);
    } else if (sold.compareTo(BigDecimal.ZERO) == -1) {
      account.setSoldType(AccountRepository.CREDIT_BALANCE);
    }
    accountRepository.save(account);
    return account;
  }
}
