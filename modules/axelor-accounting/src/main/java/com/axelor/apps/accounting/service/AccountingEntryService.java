package com.axelor.apps.accounting.service;

import com.axelor.apps.accounting.db.AccountingEntry;
import com.axelor.apps.invoice.db.Invoice;

public interface AccountingEntryService {
  AccountingEntry generateAccountingEntry(Invoice invoice);
}
