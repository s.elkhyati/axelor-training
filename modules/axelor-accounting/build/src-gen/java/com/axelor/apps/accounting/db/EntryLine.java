package com.axelor.apps.accounting.db;

import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.axelor.auth.db.AuditableModel;
import com.axelor.db.annotations.Widget;
import com.google.common.base.MoreObjects;

@Entity
@Table(name = "ACCOUNTING_ENTRY_LINE", indexes = { @Index(columnList = "account"), @Index(columnList = "accounting_entry") })
public class EntryLine extends AuditableModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACCOUNTING_ENTRY_LINE_SEQ")
	@SequenceGenerator(name = "ACCOUNTING_ENTRY_LINE_SEQ", sequenceName = "ACCOUNTING_ENTRY_LINE_SEQ", allocationSize = 1)
	private Long id;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private Account account;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private AccountingEntry accountingEntry;

	@Widget(title = "Debit")
	@DecimalMin("0")
	private BigDecimal debit = new BigDecimal("0");

	@Widget(title = "Credit")
	@DecimalMin("0")
	private BigDecimal credit = new BigDecimal("0");

	@Widget(title = "Attributes")
	@Basic(fetch = FetchType.LAZY)
	@Type(type = "json")
	private String attrs;

	public EntryLine() {
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public AccountingEntry getAccountingEntry() {
		return accountingEntry;
	}

	public void setAccountingEntry(AccountingEntry accountingEntry) {
		this.accountingEntry = accountingEntry;
	}

	public BigDecimal getDebit() {
		return debit == null ? BigDecimal.ZERO : debit;
	}

	public void setDebit(BigDecimal debit) {
		this.debit = debit;
	}

	public BigDecimal getCredit() {
		return credit == null ? BigDecimal.ZERO : credit;
	}

	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}

	public String getAttrs() {
		return attrs;
	}

	public void setAttrs(String attrs) {
		this.attrs = attrs;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (this == obj) return true;
		if (!(obj instanceof EntryLine)) return false;

		final EntryLine other = (EntryLine) obj;
		if (this.getId() != null || other.getId() != null) {
			return Objects.equals(this.getId(), other.getId());
		}

		return false;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", getId())
			.add("debit", getDebit())
			.add("credit", getCredit())
			.omitNullValues()
			.toString();
	}
}
