package com.axelor.apps.accounting.db;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.axelor.auth.db.AuditableModel;
import com.axelor.db.annotations.Sequence;
import com.axelor.db.annotations.Widget;
import com.google.common.base.MoreObjects;

@Entity
@Table(name = "ACCOUNTING_ACCOUNTING_ENTRY")
public class AccountingEntry extends AuditableModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACCOUNTING_ACCOUNTING_ENTRY_SEQ")
	@SequenceGenerator(name = "ACCOUNTING_ACCOUNTING_ENTRY_SEQ", sequenceName = "ACCOUNTING_ACCOUNTING_ENTRY_SEQ", allocationSize = 1)
	private Long id;

	@Widget(title = "Document reference")
	@Sequence("accounting.entry.seq")
	private String documentReference;

	@Widget(title = "Operation date")
	@NotNull
	private LocalDate operationDate;

	@Widget(title = "Entry lines")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "accountingEntry", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<EntryLine> entryLines;

	@Widget(title = "Total Debit", readonly = true)
	private BigDecimal totalDebit = new BigDecimal("0");

	@Widget(title = "Total Credit", readonly = true)
	private BigDecimal totalCredit = new BigDecimal("0");

	@Transient
	private Boolean entryLinesReadOnly = Boolean.TRUE;

	@Widget(title = "Attributes")
	@Basic(fetch = FetchType.LAZY)
	@Type(type = "json")
	private String attrs;

	public AccountingEntry() {
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentReference() {
		return documentReference;
	}

	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}

	public LocalDate getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(LocalDate operationDate) {
		this.operationDate = operationDate;
	}

	public List<EntryLine> getEntryLines() {
		return entryLines;
	}

	public void setEntryLines(List<EntryLine> entryLines) {
		this.entryLines = entryLines;
	}

	/**
	 * Add the given {@link EntryLine} item to the {@code entryLines}.
	 *
	 * <p>
	 * It sets {@code item.accountingEntry = this} to ensure the proper relationship.
	 * </p>
	 *
	 * @param item
	 *            the item to add
	 */
	public void addEntryLine(EntryLine item) {
		if (getEntryLines() == null) {
			setEntryLines(new ArrayList<>());
		}
		getEntryLines().add(item);
		item.setAccountingEntry(this);
	}

	/**
	 * Remove the given {@link EntryLine} item from the {@code entryLines}.
	 *
 	 * @param item
	 *            the item to remove
	 */
	public void removeEntryLine(EntryLine item) {
		if (getEntryLines() == null) {
			return;
		}
		getEntryLines().remove(item);
	}

	/**
	 * Clear the {@code entryLines} collection.
	 *
	 * <p>
	 * If you have to query {@link EntryLine} records in same transaction, make
	 * sure to call {@link javax.persistence.EntityManager#flush() } to avoid
	 * unexpected errors.
	 * </p>
	 */
	public void clearEntryLines() {
		if (getEntryLines() != null) {
			getEntryLines().clear();
		}
	}

	public BigDecimal getTotalDebit() {
		return totalDebit == null ? BigDecimal.ZERO : totalDebit;
	}

	public void setTotalDebit(BigDecimal totalDebit) {
		this.totalDebit = totalDebit;
	}

	public BigDecimal getTotalCredit() {
		return totalCredit == null ? BigDecimal.ZERO : totalCredit;
	}

	public void setTotalCredit(BigDecimal totalCredit) {
		this.totalCredit = totalCredit;
	}

	public Boolean getEntryLinesReadOnly() {
		return entryLinesReadOnly == null ? Boolean.FALSE : entryLinesReadOnly;
	}

	public void setEntryLinesReadOnly(Boolean entryLinesReadOnly) {
		this.entryLinesReadOnly = entryLinesReadOnly;
	}

	public String getAttrs() {
		return attrs;
	}

	public void setAttrs(String attrs) {
		this.attrs = attrs;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (this == obj) return true;
		if (!(obj instanceof AccountingEntry)) return false;

		final AccountingEntry other = (AccountingEntry) obj;
		if (this.getId() != null || other.getId() != null) {
			return Objects.equals(this.getId(), other.getId());
		}

		return false;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", getId())
			.add("documentReference", getDocumentReference())
			.add("operationDate", getOperationDate())
			.add("totalDebit", getTotalDebit())
			.add("totalCredit", getTotalCredit())
			.add("entryLinesReadOnly", getEntryLinesReadOnly())
			.omitNullValues()
			.toString();
	}
}
