package com.axelor.apps.accounting.db;

import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.axelor.auth.db.AuditableModel;
import com.axelor.db.annotations.Widget;
import com.google.common.base.MoreObjects;

import com.axelor.apps.accounting.db.repo.AccountRepository;

@Entity
@Table(name = "ACCOUNTING_ACCOUNT", indexes = { @Index(columnList = "code"), @Index(columnList = "name") })
public class Account extends AuditableModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACCOUNTING_ACCOUNT_SEQ")
	@SequenceGenerator(name = "ACCOUNTING_ACCOUNT_SEQ", sequenceName = "ACCOUNTING_ACCOUNT_SEQ", allocationSize = 1)
	private Long id;

	@Widget(title = "Code")
	private String code;

	@Widget(title = "Name")
	private String name;

	@Widget(title = "sold", readonly = true)
	private BigDecimal sold = new BigDecimal("0");

	@Widget(title = "sold Type", readonly = true, selection = "accounting.account.sold.tpe.select")
	private Integer soldType = AccountRepository.ZERO_BALANCE_ACCOUNT;

	@Widget(title = "Attributes")
	@Basic(fetch = FetchType.LAZY)
	@Type(type = "json")
	private String attrs;

	public Account() {
	}

	public Account(String code, String name) {
		this.code = code;
		this.name = name;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getSold() {
		return sold == null ? BigDecimal.ZERO : sold;
	}

	public void setSold(BigDecimal sold) {
		this.sold = sold;
	}

	public Integer getSoldType() {
		return soldType == null ? 0 : soldType;
	}

	public void setSoldType(Integer soldType) {
		this.soldType = soldType;
	}

	public String getAttrs() {
		return attrs;
	}

	public void setAttrs(String attrs) {
		this.attrs = attrs;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (this == obj) return true;
		if (!(obj instanceof Account)) return false;

		final Account other = (Account) obj;
		if (this.getId() != null || other.getId() != null) {
			return Objects.equals(this.getId(), other.getId());
		}

		return false;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", getId())
			.add("code", getCode())
			.add("name", getName())
			.add("sold", getSold())
			.add("soldType", getSoldType())
			.omitNullValues()
			.toString();
	}
}
