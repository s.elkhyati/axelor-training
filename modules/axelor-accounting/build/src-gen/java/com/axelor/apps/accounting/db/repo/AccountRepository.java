package com.axelor.apps.accounting.db.repo;

import com.axelor.apps.accounting.db.Account;
import com.axelor.db.JpaRepository;
import com.axelor.db.Query;

import com.axelor.apps.accounting.db.repo.AccountRepository;

public class AccountRepository extends JpaRepository<Account> {

	public AccountRepository() {
		super(Account.class);
	}

	public Account findByCode(String code) {
		return Query.of(Account.class)
				.filter("self.code = :code")
				.bind("code", code)
				.fetchOne();
	}

	public Account findByName(String name) {
		return Query.of(Account.class)
				.filter("self.name = :name")
				.bind("name", name)
				.fetchOne();
	}

	public static final Integer ZERO_BALANCE_ACCOUNT = 0;
	public static final Integer DEBIT_BALANCE = 1;
	public static final Integer CREDIT_BALANCE = 2;

	public static final String VAT_ACCOUNT = "44571";
	public static final String CLIENT_ACCOUNT = "411";
}

