package com.axelor.apps.invoice.db.repo;

import com.axelor.apps.invoice.db.InvoiceLine;
import com.axelor.db.JpaRepository;

public class InvoiceLineRepository extends JpaRepository<InvoiceLine> {

	public InvoiceLineRepository() {
		super(InvoiceLine.class);
	}

}

