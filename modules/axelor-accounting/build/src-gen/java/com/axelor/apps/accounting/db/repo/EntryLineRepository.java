package com.axelor.apps.accounting.db.repo;

import com.axelor.apps.accounting.db.EntryLine;
import com.axelor.db.JpaRepository;

public class EntryLineRepository extends JpaRepository<EntryLine> {

	public EntryLineRepository() {
		super(EntryLine.class);
	}

}

