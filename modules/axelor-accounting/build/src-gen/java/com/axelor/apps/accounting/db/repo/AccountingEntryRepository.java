package com.axelor.apps.accounting.db.repo;

import com.axelor.apps.accounting.db.AccountingEntry;
import com.axelor.db.JpaRepository;

public class AccountingEntryRepository extends JpaRepository<AccountingEntry> {

	public AccountingEntryRepository() {
		super(AccountingEntry.class);
	}

}

