package com.axelor.apps.sale.db.repo;

import com.axelor.apps.sale.db.OrderLine;
import com.axelor.db.JpaRepository;

public class OrderLineRepository extends JpaRepository<OrderLine> {

	public OrderLineRepository() {
		super(OrderLine.class);
	}

}

