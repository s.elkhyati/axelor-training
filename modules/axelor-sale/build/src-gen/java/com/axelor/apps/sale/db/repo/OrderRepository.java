package com.axelor.apps.sale.db.repo;

import com.axelor.apps.sale.db.Order;
import com.axelor.db.JpaRepository;
import com.axelor.db.Query;

import com.axelor.apps.sale.db.repo.OrderRepository;

public class OrderRepository extends JpaRepository<Order> {

	public OrderRepository() {
		super(Order.class);
	}

	public Order findByCode(String code) {
		return Query.of(Order.class)
				.filter("self.code = :code")
				.bind("code", code)
				.fetchOne();
	}

	public final static Integer DRAFT_ORDER = 0;
	public final static Integer VALIDATED_ORDER = 1;
	public final static Integer CANCELED_ORDER = 2;
}

