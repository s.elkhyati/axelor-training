package com.axelor.apps.sale.db;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.axelor.apps.contact.db.Contact;
import com.axelor.auth.db.AuditableModel;
import com.axelor.db.annotations.NameColumn;
import com.axelor.db.annotations.Sequence;
import com.axelor.db.annotations.Widget;
import com.google.common.base.MoreObjects;

import com.axelor.apps.sale.db.repo.OrderRepository;

@Entity
@Table(name = "SALE_ORDER", indexes = { @Index(columnList = "code"), @Index(columnList = "customer") })
public class Order extends AuditableModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALE_ORDER_SEQ")
	@SequenceGenerator(name = "SALE_ORDER_SEQ", sequenceName = "SALE_ORDER_SEQ", allocationSize = 1)
	private Long id;

	@Widget(readonly = true)
	@NameColumn
	@Sequence("order.seq")
	private String code;

	@Widget(title = "State", readonly = true, selection = "sale.order.state.select")
	private Integer state = OrderRepository.DRAFT_ORDER;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private Contact customer;

	@Widget(title = "Order date")
	@NotNull
	private LocalDate orderDate;

	@Widget(title = "Order confirmation date", readonly = true)
	private LocalDate orderConfirmationDate;

	@Widget(title = "Order Lines")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "orderEntity", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<OrderLine> orderLines;

	@Widget(title = "Total without tax", readonly = true)
	private BigDecimal totalWithoutTax = BigDecimal.ZERO;

	@Widget(title = "Total Tax Included", readonly = true)
	private BigDecimal totalTaxIncluded = BigDecimal.ZERO;

	@Transient
	private Boolean orderLinesAreChanged = Boolean.FALSE;

	@Widget(title = "Attributes")
	@Basic(fetch = FetchType.LAZY)
	@Type(type = "json")
	private String attrs;

	public Order() {
	}

	public Order(String code) {
		this.code = code;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getState() {
		return state == null ? 0 : state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Contact getCustomer() {
		return customer;
	}

	public void setCustomer(Contact customer) {
		this.customer = customer;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public LocalDate getOrderConfirmationDate() {
		return orderConfirmationDate;
	}

	public void setOrderConfirmationDate(LocalDate orderConfirmationDate) {
		this.orderConfirmationDate = orderConfirmationDate;
	}

	public List<OrderLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(List<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}

	/**
	 * Add the given {@link OrderLine} item to the {@code orderLines}.
	 *
	 * <p>
	 * It sets {@code item.orderEntity = this} to ensure the proper relationship.
	 * </p>
	 *
	 * @param item
	 *            the item to add
	 */
	public void addOrderLine(OrderLine item) {
		if (getOrderLines() == null) {
			setOrderLines(new ArrayList<>());
		}
		getOrderLines().add(item);
		item.setOrderEntity(this);
	}

	/**
	 * Remove the given {@link OrderLine} item from the {@code orderLines}.
	 *
 	 * @param item
	 *            the item to remove
	 */
	public void removeOrderLine(OrderLine item) {
		if (getOrderLines() == null) {
			return;
		}
		getOrderLines().remove(item);
	}

	/**
	 * Clear the {@code orderLines} collection.
	 *
	 * <p>
	 * If you have to query {@link OrderLine} records in same transaction, make
	 * sure to call {@link javax.persistence.EntityManager#flush() } to avoid
	 * unexpected errors.
	 * </p>
	 */
	public void clearOrderLines() {
		if (getOrderLines() != null) {
			getOrderLines().clear();
		}
	}

	public BigDecimal getTotalWithoutTax() {
		return totalWithoutTax == null ? BigDecimal.ZERO : totalWithoutTax;
	}

	public void setTotalWithoutTax(BigDecimal totalWithoutTax) {
		this.totalWithoutTax = totalWithoutTax;
	}

	public BigDecimal getTotalTaxIncluded() {
		return totalTaxIncluded == null ? BigDecimal.ZERO : totalTaxIncluded;
	}

	public void setTotalTaxIncluded(BigDecimal totalTaxIncluded) {
		this.totalTaxIncluded = totalTaxIncluded;
	}

	public Boolean getOrderLinesAreChanged() {
		return orderLinesAreChanged == null ? Boolean.FALSE : orderLinesAreChanged;
	}

	public void setOrderLinesAreChanged(Boolean orderLinesAreChanged) {
		this.orderLinesAreChanged = orderLinesAreChanged;
	}

	public String getAttrs() {
		return attrs;
	}

	public void setAttrs(String attrs) {
		this.attrs = attrs;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (this == obj) return true;
		if (!(obj instanceof Order)) return false;

		final Order other = (Order) obj;
		if (this.getId() != null || other.getId() != null) {
			return Objects.equals(this.getId(), other.getId());
		}

		return false;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", getId())
			.add("code", getCode())
			.add("state", getState())
			.add("orderDate", getOrderDate())
			.add("orderConfirmationDate", getOrderConfirmationDate())
			.add("totalWithoutTax", getTotalWithoutTax())
			.add("totalTaxIncluded", getTotalTaxIncluded())
			.add("orderLinesAreChanged", getOrderLinesAreChanged())
			.omitNullValues()
			.toString();
	}
}
