package com.axelor.apps.invoice.db;

import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.axelor.apps.accounting.db.Account;
import com.axelor.apps.sale.db.Product;
import com.axelor.auth.db.AuditableModel;
import com.axelor.db.annotations.VirtualColumn;
import com.axelor.db.annotations.Widget;
import com.google.common.base.MoreObjects;

@Entity
@Table(name = "INVOICE_INVOICE_LINE", indexes = { @Index(columnList = "invoice"), @Index(columnList = "product"), @Index(columnList = "account") })
public class InvoiceLine extends AuditableModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INVOICE_INVOICE_LINE_SEQ")
	@SequenceGenerator(name = "INVOICE_INVOICE_LINE_SEQ", sequenceName = "INVOICE_INVOICE_LINE_SEQ", allocationSize = 1)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private Invoice invoice;

	@Widget(title = "Product")
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private Product product;

	@Widget(title = "Description")
	private String description;

	@Widget(title = "Qte")
	@NotNull
	@DecimalMin("1")
	private BigDecimal quantity = new BigDecimal("1");

	@Widget(title = "Unit price without tax")
	private BigDecimal unitPriceWithoutTax = BigDecimal.ZERO;

	@Widget(title = "subtotal without tax", readonly = true)
	private BigDecimal subtotalWithoutTax = BigDecimal.ZERO;

	private BigDecimal valueAddedTax = BigDecimal.ZERO;

	@Widget(title = "subtotal tax included", readonly = true)
	private BigDecimal subtotalTaxIncluded = BigDecimal.ZERO;

	@Widget(readonly = false)
	@VirtualColumn
	@Access(AccessType.PROPERTY)
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private Account account;

	@Widget(title = "Attributes")
	@Basic(fetch = FetchType.LAZY)
	@Type(type = "json")
	private String attrs;

	public InvoiceLine() {
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getQuantity() {
		return quantity == null ? BigDecimal.ZERO : quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPriceWithoutTax() {
		return unitPriceWithoutTax == null ? BigDecimal.ZERO : unitPriceWithoutTax;
	}

	public void setUnitPriceWithoutTax(BigDecimal unitPriceWithoutTax) {
		this.unitPriceWithoutTax = unitPriceWithoutTax;
	}

	public BigDecimal getSubtotalWithoutTax() {
		return subtotalWithoutTax == null ? BigDecimal.ZERO : subtotalWithoutTax;
	}

	public void setSubtotalWithoutTax(BigDecimal subtotalWithoutTax) {
		this.subtotalWithoutTax = subtotalWithoutTax;
	}

	public BigDecimal getValueAddedTax() {
		return valueAddedTax == null ? BigDecimal.ZERO : valueAddedTax;
	}

	public void setValueAddedTax(BigDecimal valueAddedTax) {
		this.valueAddedTax = valueAddedTax;
	}

	public BigDecimal getSubtotalTaxIncluded() {
		return subtotalTaxIncluded == null ? BigDecimal.ZERO : subtotalTaxIncluded;
	}

	public void setSubtotalTaxIncluded(BigDecimal subtotalTaxIncluded) {
		this.subtotalTaxIncluded = subtotalTaxIncluded;
	}

	public Account getAccount() {
		try {
			account = computeAccount();
		} catch (NullPointerException e) {
			Logger logger = LoggerFactory.getLogger(getClass());
			logger.error("NPE in function field: getAccount()", e);
		}
		return account;
	}

	protected Account computeAccount() {
		if(product != null && product.getCategory() != null){
		return product.getCategory().getAccount();
		 }
		 return null;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public String getAttrs() {
		return attrs;
	}

	public void setAttrs(String attrs) {
		this.attrs = attrs;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (this == obj) return true;
		if (!(obj instanceof InvoiceLine)) return false;

		final InvoiceLine other = (InvoiceLine) obj;
		if (this.getId() != null || other.getId() != null) {
			return Objects.equals(this.getId(), other.getId());
		}

		return false;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", getId())
			.add("description", getDescription())
			.add("quantity", getQuantity())
			.add("unitPriceWithoutTax", getUnitPriceWithoutTax())
			.add("subtotalWithoutTax", getSubtotalWithoutTax())
			.add("valueAddedTax", getValueAddedTax())
			.add("subtotalTaxIncluded", getSubtotalTaxIncluded())
			.omitNullValues()
			.toString();
	}
}
