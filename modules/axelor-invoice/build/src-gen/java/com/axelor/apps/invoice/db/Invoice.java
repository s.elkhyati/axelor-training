package com.axelor.apps.invoice.db;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.axelor.apps.contact.db.Contact;
import com.axelor.auth.db.AuditableModel;
import com.axelor.db.annotations.NameColumn;
import com.axelor.db.annotations.Sequence;
import com.axelor.db.annotations.VirtualColumn;
import com.axelor.db.annotations.Widget;
import com.google.common.base.MoreObjects;

import com.axelor.apps.invoice.db.repo.InvoiceRepository;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "INVOICE_INVOICE", indexes = { @Index(columnList = "code"), @Index(columnList = "customer") })
public class Invoice extends AuditableModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INVOICE_INVOICE_SEQ")
	@SequenceGenerator(name = "INVOICE_INVOICE_SEQ", sequenceName = "INVOICE_INVOICE_SEQ", allocationSize = 1)
	private Long id;

	@Widget(readonly = true)
	@NameColumn
	@Sequence("invoice.seq")
	private String code;

	@Widget(readonly = true, selection = "invoice.invoice.state.select")
	private Integer state = 0;

	@Widget(title = "Customer")
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private Contact customer;

	@Widget(title = "Invoice date")
	@VirtualColumn
	@Access(AccessType.PROPERTY)
	private LocalDate invoiceDate;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "invoice", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<InvoiceLine> invoiceLines;

	@Widget(title = "Total without tax", readonly = true)
	private BigDecimal totalWithoutTax = BigDecimal.ZERO;

	@Widget(title = "total Tax included", readonly = true)
	private BigDecimal totalTaxIncluded = BigDecimal.ZERO;

	@Transient
	private Boolean invoiceLinesAreChanged = Boolean.FALSE;

	@Widget(title = "Attributes")
	@Basic(fetch = FetchType.LAZY)
	@Type(type = "json")
	private String attrs;

	public Invoice() {
	}

	public Invoice(String code) {
		this.code = code;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getState() {
		return state == null ? 0 : state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Contact getCustomer() {
		return customer;
	}

	public void setCustomer(Contact customer) {
		this.customer = customer;
	}

	public LocalDate getInvoiceDate() {
		try {
			invoiceDate = computeInvoiceDate();
		} catch (NullPointerException e) {
			Logger logger = LoggerFactory.getLogger(getClass());
			logger.error("NPE in function field: getInvoiceDate()", e);
		}
		return invoiceDate;
	}

	protected LocalDate computeInvoiceDate() {
		return LocalDate.now();
	}

	public void setInvoiceDate(LocalDate invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public List<InvoiceLine> getInvoiceLines() {
		return invoiceLines;
	}

	public void setInvoiceLines(List<InvoiceLine> invoiceLines) {
		this.invoiceLines = invoiceLines;
	}

	/**
	 * Add the given {@link InvoiceLine} item to the {@code invoiceLines}.
	 *
	 * <p>
	 * It sets {@code item.invoice = this} to ensure the proper relationship.
	 * </p>
	 *
	 * @param item
	 *            the item to add
	 */
	public void addInvoiceLine(InvoiceLine item) {
		if (getInvoiceLines() == null) {
			setInvoiceLines(new ArrayList<>());
		}
		getInvoiceLines().add(item);
		item.setInvoice(this);
	}

	/**
	 * Remove the given {@link InvoiceLine} item from the {@code invoiceLines}.
	 *
 	 * @param item
	 *            the item to remove
	 */
	public void removeInvoiceLine(InvoiceLine item) {
		if (getInvoiceLines() == null) {
			return;
		}
		getInvoiceLines().remove(item);
	}

	/**
	 * Clear the {@code invoiceLines} collection.
	 *
	 * <p>
	 * If you have to query {@link InvoiceLine} records in same transaction, make
	 * sure to call {@link javax.persistence.EntityManager#flush() } to avoid
	 * unexpected errors.
	 * </p>
	 */
	public void clearInvoiceLines() {
		if (getInvoiceLines() != null) {
			getInvoiceLines().clear();
		}
	}

	public BigDecimal getTotalWithoutTax() {
		return totalWithoutTax == null ? BigDecimal.ZERO : totalWithoutTax;
	}

	public void setTotalWithoutTax(BigDecimal totalWithoutTax) {
		this.totalWithoutTax = totalWithoutTax;
	}

	public BigDecimal getTotalTaxIncluded() {
		return totalTaxIncluded == null ? BigDecimal.ZERO : totalTaxIncluded;
	}

	public void setTotalTaxIncluded(BigDecimal totalTaxIncluded) {
		this.totalTaxIncluded = totalTaxIncluded;
	}

	public Boolean getInvoiceLinesAreChanged() {
		return invoiceLinesAreChanged == null ? Boolean.FALSE : invoiceLinesAreChanged;
	}

	public void setInvoiceLinesAreChanged(Boolean invoiceLinesAreChanged) {
		this.invoiceLinesAreChanged = invoiceLinesAreChanged;
	}

	public String getAttrs() {
		return attrs;
	}

	public void setAttrs(String attrs) {
		this.attrs = attrs;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (this == obj) return true;
		if (!(obj instanceof Invoice)) return false;

		final Invoice other = (Invoice) obj;
		if (this.getId() != null || other.getId() != null) {
			return Objects.equals(this.getId(), other.getId());
		}

		return false;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", getId())
			.add("code", getCode())
			.add("state", getState())
			.add("totalWithoutTax", getTotalWithoutTax())
			.add("totalTaxIncluded", getTotalTaxIncluded())
			.add("invoiceLinesAreChanged", getInvoiceLinesAreChanged())
			.omitNullValues()
			.toString();
	}
}
