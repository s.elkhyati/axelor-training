package com.axelor.apps.invoice.db.repo;

import com.axelor.apps.invoice.db.Invoice;
import com.axelor.db.JpaRepository;
import com.axelor.db.Query;

import com.axelor.apps.invoice.db.repo.InvoiceRepository;

public class InvoiceRepository extends JpaRepository<Invoice> {

	public InvoiceRepository() {
		super(Invoice.class);
	}

	public Invoice findByCode(String code) {
		return Query.of(Invoice.class)
				.filter("self.code = :code")
				.bind("code", code)
				.fetchOne();
	}

	public final static Integer DRAFT_INVOICE = 0;
	public final static Integer VALIDATED_INVOICE= 1;
}

