package com.axelor.apps.invoice.web;

import com.axelor.apps.invoice.db.Invoice;
import com.axelor.apps.invoice.service.InvoiceService;
import com.axelor.apps.sale.db.Order;
import com.axelor.apps.sale.db.repo.OrderRepository;
import com.axelor.rpc.ActionRequest;
import com.axelor.rpc.ActionResponse;
import com.google.inject.Inject;
import java.time.LocalDate;

public class OrderController {

  @Inject InvoiceService invoiceService;

  public void setEstimatedBillingDate(ActionRequest request, ActionResponse response) {
    Order order = request.getContext().asType(Order.class);
    if (order != null) {
      LocalDate orderConfirmationDate = order.getOrderConfirmationDate();
      LocalDate estimatedBillingDate = orderConfirmationDate.plusDays(30);
      response.setValue("estimatedBillingDate", estimatedBillingDate);
    }
  }

  public void generateInvoice(ActionRequest request, ActionResponse response) {
    Order order = request.getContext().asType(Order.class);
    if (order != null && order.getOrderLines() != null && !order.getOrderLines().isEmpty()) {
      Invoice invoice = invoiceService.generateInvoice(order);
      if (invoice != null) {
        response.setNotify("Invoice has been created");
        response.setValue("state", OrderRepository.INVOICED_ORDER);
        response.setValue("generatedInvoice", invoice);
        LocalDate invoiceDate = LocalDate.now();
        response.setValue("invoiceDate", invoiceDate);
      }
    }
  }

  public void invoiceLateOrders(ActionRequest request, ActionResponse response) {
    int invoicedOrders = invoiceService.invoiceNotInvoicedOrders();
    if (invoicedOrders > 0) {
      response.setNotify("Number of invoiced orders is: " + invoicedOrders);
      response.setReload(true);
      return;
    }
    response.setNotify("all orders are already invoiced");
  }
}
