package com.axelor.apps.invoice.service;

import com.axelor.apps.invoice.db.Invoice;
import com.axelor.apps.invoice.db.InvoiceLine;
import com.axelor.apps.invoice.db.repo.InvoiceRepository;
import com.axelor.apps.sale.db.Order;
import com.axelor.apps.sale.db.OrderLine;
import com.axelor.apps.sale.db.repo.OrderRepository;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class InvoiceServiceImpl implements InvoiceService {

  @Inject InvoiceRepository invoiceRepository;
  @Inject OrderRepository orderRepository;

  @Override
  public boolean calculateInvoiceTotalPrice(Invoice invoice) {
    BigDecimal totalWithoutTax = BigDecimal.valueOf(0);
    BigDecimal totalTaxIncluded = BigDecimal.valueOf(0);
    List<InvoiceLine> invoiceLineList = invoice.getInvoiceLines();
    if (invoiceLineList != null) {
      for (InvoiceLine invoiceLine : invoiceLineList) {
        totalWithoutTax = totalWithoutTax.add(invoiceLine.getSubtotalWithoutTax());
        totalTaxIncluded = totalTaxIncluded.add(invoiceLine.getSubtotalTaxIncluded());
        invoice.setTotalTaxIncluded(totalTaxIncluded);
        invoice.setTotalWithoutTax(totalWithoutTax);
      }
      return true;
    }
    return false;
  }

  @Override
  @Transactional
  public Invoice generateInvoice(Order order) {
    Invoice invoice = new Invoice();
    invoice.setCustomer(order.getCustomer());
    invoice.setInvoiceDate(LocalDate.now());
    invoice.setState(InvoiceRepository.DRAFT_INVOICE);
    invoice.setTotalWithoutTax(order.getTotalWithoutTax());
    invoice.setTotalTaxIncluded(order.getTotalTaxIncluded());
    List<InvoiceLine> invoiceLineList = new ArrayList<>();
    for (OrderLine orderLine : order.getOrderLines()) {
      InvoiceLine invoiceLine = new InvoiceLine();
      invoiceLine.setInvoice(invoice);
      invoiceLine.setProduct(orderLine.getProduct());
      invoiceLine.setDescription(orderLine.getDescription());
      invoiceLine.setQuantity(orderLine.getQuantity());
      invoiceLine.setUnitPriceWithoutTax(orderLine.getUnitPriceWithoutTax());
      invoiceLine.setSubtotalWithoutTax(orderLine.getSubtotalWithoutTax());
      invoiceLine.setValueAddedTax(orderLine.getValueAddedTax());
      invoiceLine.setSubtotalTaxIncluded(orderLine.getSubtotalTaxIncluded());
      invoiceLineList.add(invoiceLine);
    }
    invoice.setInvoiceLines(invoiceLineList);
    invoiceRepository.save(invoice);
    return invoice;
  }

  @Override
  @Transactional
  public int invoiceNotInvoicedOrders() {
    List<Order> orderList = orderRepository.getNonInvoicedOrders();
    int total = 0;
    for (Order order : orderList) {
      Invoice invoice = generateInvoice(order);
      if (invoice != null) {
        order.setState(OrderRepository.INVOICED_ORDER);
        LocalDate invoiceDate = LocalDate.now();
        order.setInvoiceDate(invoiceDate);
        order.setGeneratedInvoice(invoice);
        orderRepository.save(order);
        total++;
      }
    }
    return total;
  }
}
