package com.axelor.apps.invoice.service;

import com.axelor.apps.invoice.db.Invoice;
import com.axelor.apps.sale.db.Order;

public interface InvoiceService {
  boolean calculateInvoiceTotalPrice(Invoice invoice);

  Invoice generateInvoice(Order order);

  int invoiceNotInvoicedOrders();
}
