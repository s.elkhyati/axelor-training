package com.axelor.apps.invoice.web;

import com.axelor.apps.invoice.db.InvoiceLine;
import com.axelor.apps.invoice.service.InvoiceLineService;
import com.axelor.apps.sale.db.Product;
import com.axelor.rpc.ActionRequest;
import com.axelor.rpc.ActionResponse;
import com.google.inject.Inject;

public class InvoiceLineController {

  @Inject private InvoiceLineService invoiceLineService;

  public void onProductChangeHandler(ActionRequest request, ActionResponse response) {
    InvoiceLine invoiceLine = request.getContext().asType(InvoiceLine.class);
    Product product = invoiceLine.getProduct();
    if (invoiceLine != null && product != null) {
      invoiceLineService.setInvoiceLineProduct(invoiceLine, product);
      response.setValue("description", invoiceLine.getDescription());
      response.setValue("unitPriceWithoutTax", invoiceLine.getUnitPriceWithoutTax());
      response.setValue("subtotalWithoutTax", invoiceLine.getSubtotalWithoutTax());
      response.setValue("valueAddedTax", invoiceLine.getValueAddedTax());
      response.setValue("subtotalTaxIncluded", invoiceLine.getSubtotalTaxIncluded());
    }
  }

  public void onRecordChangeHandler(ActionRequest request, ActionResponse response) {
    InvoiceLine invoiceLine = request.getContext().asType(InvoiceLine.class);
    if (invoiceLine != null && invoiceLine.getProduct() != null) {
      invoiceLineService.calculateInvoiceLineFields(invoiceLine);
      response.setValue("subtotalWithoutTax", invoiceLine.getSubtotalWithoutTax());
      response.setValue("subtotalTaxIncluded", invoiceLine.getSubtotalTaxIncluded());
    }
  }
}
