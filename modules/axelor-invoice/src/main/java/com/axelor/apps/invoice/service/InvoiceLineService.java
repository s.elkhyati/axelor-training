package com.axelor.apps.invoice.service;

import com.axelor.apps.invoice.db.InvoiceLine;
import com.axelor.apps.sale.db.Product;

public interface InvoiceLineService {

  void setInvoiceLineProduct(InvoiceLine invoiceLine, Product product);

  void calculateInvoiceLineFields(InvoiceLine invoiceLine);
}
