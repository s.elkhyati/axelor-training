package com.axelor.apps.invoice.module;

import com.axelor.app.AxelorModule;
import com.axelor.apps.invoice.service.InvoiceLineService;
import com.axelor.apps.invoice.service.InvoiceLineServiceImpl;
import com.axelor.apps.invoice.service.InvoiceService;
import com.axelor.apps.invoice.service.InvoiceServiceImpl;

public class InvoiceModule extends AxelorModule {

  @Override
  protected void configure() {
    bind(InvoiceLineService.class).to(InvoiceLineServiceImpl.class);
    bind(InvoiceService.class).to(InvoiceServiceImpl.class);
  }
}
