package com.axelor.apps.invoice.utility;

import java.math.BigDecimal;

public class VatCalculator {

  private BigDecimal vat = BigDecimal.valueOf(0);
  private BigDecimal one = BigDecimal.ONE;
  private BigDecimal hundred = BigDecimal.valueOf(100);

  public void setVat(BigDecimal vat) {
    this.vat = vat;
  }

  public BigDecimal calculatePriceTaxIncluded(BigDecimal price) {
    return price.multiply(one.add(vatDecimal()));
  }

  public BigDecimal calculateVatPrice(BigDecimal price) {
    return price.multiply(vatDecimal());
  }

  private BigDecimal vatDecimal() {
    return vat.divide(hundred);
  }
}
