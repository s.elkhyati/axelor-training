package com.axelor.apps.invoice.service;

import com.axelor.apps.invoice.db.InvoiceLine;
import com.axelor.apps.invoice.utility.VatCalculator;
import com.axelor.apps.sale.db.Product;
import com.google.inject.Inject;
import java.math.BigDecimal;

public class InvoiceLineServiceImpl implements InvoiceLineService {

  @Inject protected VatCalculator calculator;

  public void setInvoiceLineProduct(InvoiceLine invoiceLine, Product product) {
    BigDecimal quantity = invoiceLine.getQuantity();
    BigDecimal subtotalWithoutTax = product.getUnitPriceWithoutTax().multiply(quantity);
    calculator.setVat(product.getValueAddedTax());
    BigDecimal subtotalTaxIncluded = calculator.calculatePriceTaxIncluded(subtotalWithoutTax);
    invoiceLine.setProduct(product);
    invoiceLine.setDescription(product.getDescription());
    invoiceLine.setUnitPriceWithoutTax(product.getUnitPriceWithoutTax());
    invoiceLine.setValueAddedTax(product.getValueAddedTax());
    invoiceLine.setSubtotalWithoutTax(subtotalWithoutTax);
    invoiceLine.setSubtotalTaxIncluded(subtotalTaxIncluded);
  }

  @Override
  public void calculateInvoiceLineFields(InvoiceLine invoiceLine) {
    calculator.setVat(invoiceLine.getValueAddedTax());
    BigDecimal subtotalWithoutTax =
        invoiceLine.getUnitPriceWithoutTax().multiply(invoiceLine.getQuantity());
    BigDecimal subtotalTaxIncluded = calculator.calculatePriceTaxIncluded(subtotalWithoutTax);
    invoiceLine.setSubtotalWithoutTax(subtotalWithoutTax);
    invoiceLine.setSubtotalTaxIncluded(subtotalTaxIncluded);
  }
}
