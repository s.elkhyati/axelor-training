package com.axelor.apps.invoice.web;

import com.axelor.apps.invoice.db.Invoice;
import com.axelor.apps.invoice.db.repo.InvoiceRepository;
import com.axelor.apps.invoice.service.InvoiceService;
import com.axelor.rpc.ActionRequest;
import com.axelor.rpc.ActionResponse;
import com.google.inject.Inject;
import java.math.BigDecimal;

public class InvoiceController {

  @Inject InvoiceService invoiceService;

  public void calculateTotalPrice(ActionRequest request, ActionResponse response) {
    Invoice invoice = request.getContext().asType(Invoice.class);
    if (invoice != null) {
      if (invoiceService.calculateInvoiceTotalPrice(invoice)) {
        response.setValue("totalWithoutTax", invoice.getTotalWithoutTax());
        response.setValue("totalTaxIncluded", invoice.getTotalTaxIncluded());
        response.setValue("invoiceLinesAreChanged", false);
      }
    }
  }

  public void onInvoiceLinesChange(ActionRequest request, ActionResponse response) {
    Invoice invoice = request.getContext().asType(Invoice.class);
    if (invoice != null) {
      response.setValue("invoiceLinesAreChanged", true);
    }
  }

  public void invoiceValidate(ActionRequest request, ActionResponse response) {
    Invoice invoice = request.getContext().asType(Invoice.class);
    if (invoice != null) {
      if (invoice.getCustomer() == null) {
        response.setError("Please specify the customer");
        return;
      }
      if (invoice.getTotalTaxIncluded().equals(BigDecimal.ZERO)
          || invoice.getTotalWithoutTax().equals(BigDecimal.ZERO)) {
        response.setError(
            "Please calculate the invoice total price before validate. Make sure the total is not zero");
        return;
      }
      if (invoice.getInvoiceLinesAreChanged()) {
        response.setError(
            "The invoice lines have been changed, please recalculate the order total price");
        return;
      }
      response.setValue("state", InvoiceRepository.VALIDATED_INVOICE);
    }
  }
}
