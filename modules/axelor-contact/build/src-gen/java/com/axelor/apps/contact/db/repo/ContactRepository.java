package com.axelor.apps.contact.db.repo;

import com.axelor.apps.contact.db.Contact;
import com.axelor.db.JpaRepository;

public class ContactRepository extends JpaRepository<Contact> {

	public ContactRepository() {
		super(Contact.class);
	}

}

