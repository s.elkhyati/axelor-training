package com.axelor.apps.sale.db.repo;

import com.axelor.apps.sale.db.Order;
import com.axelor.db.JpaRepository;
import com.axelor.db.Query;

import com.axelor.apps.sale.db.repo.OrderRepository;import java.util.List;

public class OrderRepository extends JpaRepository<Order> {

	public OrderRepository() {
		super(Order.class);
	}

	public Order findByCode(String code) {
		return Query.of(Order.class)
				.filter("self.code = :code")
				.bind("code", code)
				.fetchOne();
	}

	public final static Integer DRAFT_ORDER = 0;
	public final static Integer VALIDATED_ORDER = 1;
	public final static Integer CANCELED_ORDER = 2;

	public static final Integer INVOICED_ORDER = 3;
	public List<Order> getNonInvoicedOrders(){
	  Query<Order> orderQuery = Query.of(Order.class).filter("self.state = :validatedOrder AND self.estimatedBillingDate < CURRENT_DATE ").bind("validatedOrder", VALIDATED_ORDER );
	  return orderQuery.fetch();
	}
}

